import os

# defaults
class Config(object):
	DEBUG = False
	THREADED = True
	SQLALCHEMY_TRACK_MODIFICATIONS = True

class Development(Config):
	DEBUG = True

class Production(Config):
	DEBUG = False