from os import environ
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object("config." + environ['ENVIRONMENT'])

db = SQLAlchemy(app)

from models import *

@app.route('/', methods = ['GET'])
def main():
	return "HELLO WORLD"

if __name__ == '__main__':
	app.run()